﻿using NoteXml.Models;
using System.Web.Mvc;
using System.Xml;

namespace NoteXml.Controllers
{

    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            return View();
        }
        public ActionResult Notes()
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../noteXml.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("notes");
            XmlNodeList note = RootNode.ChildNodes;

          
            return View(note);

        }
        public ActionResult Add(Note n)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../noteXml.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("notes");

            int noteNumber = RootNode.ChildNodes.Count;
            XmlNode noteNode = RootNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "note", ""));
            noteNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Id", "")).InnerText = noteNumber.ToString();
            noteNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Date", "")).InnerText = n.date.ToString();
            noteNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Title", "")).InnerText = n.title;
            noteNode.AppendChild(XmlDoc.CreateNode(XmlNodeType.Element, "Note", "")).InnerText = n.note;
            

            XmlDoc.Save(Server.MapPath("../noteXml.xml"));


            return RedirectToAction("Index");

        }
        public ActionResult Get(string id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../noteXml.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("notes");
            XmlNodeList notes = RootNode.ChildNodes;

            XmlNode n = notes[0];

            foreach (XmlNode no in notes)
            {
                if (no["Id"].InnerText == id)
                {
                    n = no;
                }
            }

            return View(n);

        }
        public ActionResult Edit(string id)
        {
            XmlDocument XmlDoc = new XmlDocument();
            XmlDoc.Load(Server.MapPath("../../noteXml.xml"));
            XmlNode RootNode = XmlDoc.SelectSingleNode("notes");
            XmlNodeList notes = RootNode.ChildNodes;

            XmlNode n = notes[0];

            foreach (XmlNode no in notes)
            {
                if (no["Id"].InnerText == id)
                {
                    n = no;
                }
            }

            return View(n);

        }
        public ActionResult Update(Note n)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../noteXml.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("notes");
            XmlNodeList notes = RootNode.ChildNodes;

            XmlNode note = notes[0];

            foreach (XmlNode item in notes)
            {
                if (item["Id"].InnerText == n.id.ToString())
                {
                    note = item;
                }
            }

            note["Date"].InnerText = n.date;
            note["Title"].InnerText = n.title;
            note["Note"].InnerText = n.note;

            XmlDocObj.Save(Server.MapPath("../noteXml.xml"));

            return RedirectToAction("Notes");

        }
        public ActionResult Delete(string id)
        {
            XmlDocument XmlDocObj = new XmlDocument();
            XmlDocObj.Load(Server.MapPath("../../noteXml.xml"));
            XmlNode RootNode = XmlDocObj.SelectSingleNode("notes");
            XmlNodeList no = RootNode.ChildNodes;

            XmlNode n = no[0];

            foreach (XmlNode note in no)
            {
                if (note["Id"].InnerText == id)
                {
                    n = note;
                }
            }

            RootNode.RemoveChild(n);

            XmlDocObj.Save(Server.MapPath("../../noteXml.xml"));

            return RedirectToAction("Notes");
        }
    }
}