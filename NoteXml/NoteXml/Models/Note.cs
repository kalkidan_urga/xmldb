﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NoteXml.Models
{
    public class Note
    {
        [Required]
        public int id { get; set; }
        public string date { get; set; }
        [Required]
        public string title { get; set; }
        [Required]

        public string note { get; set; }
    }
}